package ua.util

import org.codehaus.groovy.tools.shell.util.Logger

class Hello {
    private static Logger log = new Logger("Hello.class")

    static String sayHello(String name) {
        log.error("test Error ${name}")
        return "Hello ${name}"
    }

    String hello(String name) {
        return "Hello from method ${name}"
    }
}
