def info(String message){
    echo "INFO: ${message}"
}

def error(String message){
    echo "ERROR: ${message}"
}

def warn(String message){
    echo "WARN: ${message}"
}