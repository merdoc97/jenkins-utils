import ua.util.Hello

def call(Map pipelineParams) {
    def hello = new Hello()
    pipeline {
        agent {
            label 'kubernetes-jenkins'
        }
        environment {
            JAVA_HOME = "/home/jenkins/jdk-13.0.2"
            GROOVY_HOME = "/home/jenkins/tools/hudson.plugins.groovy.GroovyInstallation/groovy"
            M2_HOME = "${tool 'maven-3.6.3'}"
            PATH = "${env.JAVA_HOME}/bin:${env.PATH}:${env.GROOVY_HOME}/bin"
        }
        stages() {
            stage("check var") {
                steps {
                    println(hello.hello("Jenkins"))
                }
            }
        }
    }
}