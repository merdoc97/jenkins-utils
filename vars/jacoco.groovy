def generateReport(String mavenModule, String generateReport) {
    def pathToModule = "/";
    if (mavenModule != null) {
        pathToModule = "${mavenModule}"
    }
    if (generateReport=="true") {
        log.info("starting generate jacoco report for path ${pathToModule}")
        step([$class          : 'JacocoPublisher',
              execPattern     : "${env.mavenModule}target/*.exec",
              classPattern    : "${env.mavenModule}target/classes",
              sourcePattern   : "${env.mavenModule}src/main/java",
              exclusionPattern: "${env.mavenModule}src/test*"
        ])
    }
}
