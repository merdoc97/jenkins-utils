def call(Map pipelineParams) {
    pipeline {
        agent {
            label 'kubernetes-jenkins'
        }
        environment {
            JAVA_HOME = "/home/jenkins/jdk-17.0.1"
            M2_HOME = "${tool 'maven-3.6.3'}"
            PATH = "${env.JAVA_HOME}/bin:${env.PATH}"
            branch = "${pipelineParams.branch}"
            scmUrl = "${pipelineParams.scmUrl}"
            credentialsId = "jenkins"
            mavenModule = "${pipelineParams.mavenModule}"
            dockerRegistry = "localhost:5000"
            pathToAppChart = "${pipelineParams.pathToAppChart}"
            helmReleaseName = "${pipelineParams.helmReleaseName}"
            kubernetesNameSpace = "${pipelineParams.kubernetesNameSpace}"
            generateReport = "${pipelineParams.generateReport}"
            sonarPluginName = "sonar"
            useSonar = "${pipelineParams.useSonar}"
            tagOnly = "${pipelineParams.tagOnly}"
        }

        stages {
            stage('Check out') {
                steps {
                    script {
                        git.gitCheckout("${env.branch}", "${env.credentialsId}", "${env.scmUrl}")
                    }
                }
            }
            stage('Maven build') {
                steps {
                    script {
                        maven.mavenPackage("${env.mavenModule}")
                    }
                }
            }
            stage('Maven test') {
                steps {
                    script {
                        maven.mavenTest("${env.mavenModule}")
                    }
                }
            }
            stage('Generate test report') {
                steps {
                    script {
                        jacoco.generateReport("${env.mavenModule}", "${env.generateReport}")
                    }
                }
            }
            stage('Sonar analyst and quality gate') {
                steps {
                    script {
                        if ("${env.useSonar}" == 'true')
                            sonar.sonarAnalystAndQualityGates("${env.WORKSPACE}", "${env.mavenModule}", "${env.sonarPluginName}")
                    }
                }
            }
            stage('prepare release branch') {
                steps {
                    script {
                        currentVersion = maven.getCurrentSnapshotVersion("${env.mavenModule}")
                        log.info("current version: " + currentVersion)
                        newVersion = maven.splitVersionSnapshot(currentVersion)
                        log.info("new release version is " + newVersion)
                        if ("${env.tagOnly}" == 'true') {
                            maven.changeVersion(newVersion, "${env.mavenModule}")
                            git.commit("create release version " + newVersion)
                            git.addTag(newVersion)
                            git.push("${env.credentialsId}", "${env.branch}")
                            git.pushTag("${env.credentialsId}")
                        } else {
                            log.info("ignore creating release branch")
                        }
                    }
                }
            }
            stage('Maven docker build') {
                steps {
                    script {
                        maven.buildDockerImage("${env.mavenModule}", "${env.dockerRegistry}", git.getLastCommit())
                    }
                }
            }
            stage('Maven push to registry') {
                steps {
                    script {
                        maven.pushToRegistry("${env.mavenModule}", "${env.dockerRegistry}", git.getLastCommit())
                    }
                }
            }
            stage('Upgrade version in chart') {
                steps {
                    script {
                        helm.upgradeChartVersion("${env.pathToAppChart}/Chart.yaml", git.getLastCommit())
                        currentVersion = maven.getCurrentNotSnapshot("${env.mavenModule}")
                        helm.upgradeVersion("${env.pathToAppChart}/Chart.yaml", currentVersion)
                    }
                }
            }
            stage('Helm upgrade') {
                steps {
                    script {
                        helm.upgradeHelm("${env.helmReleaseName}", "${env.pathToAppChart}", "${env.kubernetesNameSpace}")
                    }
                }
            }

            stage('Create new Snapshot version') {
                steps {
                    script {
                        currentVersion = maven.getCurrentNotSnapshot("${env.mavenModule}")
                        log.info("current version: " + currentVersion)
                        newVersion = maven.incrementVersion(currentVersion)
                        maven.changeSnapshotVersion(newVersion, "${env.mavenModule}")
                        log.info("commit and push new version: " + newVersion)
                        git.commit("update snapshot version to " + newVersion)
                        git.push("${env.credentialsId}", "${env.branch}")
                    }
                }
            }

        }
    }

}