import static ua.util.HelmUtil.isContainRelease

def upgradeChartVersion(String pathTo, String version) {
    log.info("starting upgrade appVersion in chart for file ${pathTo} for version ${version}")
    def data = readYaml file: ".${pathTo}"
    data.appVersion = version
    log.info("new data in yaml after upgrade")
    writeYaml file: ".${pathTo}", overwrite: true, data: data
}

def upgradeVersion(String pathTo, String version){
    log.info("starting upgrade version in chart for file ${pathTo} for version ${version}")
    def data = readYaml file: ".${pathTo}"
    data.version = version
    log.info("new data in yaml after upgrade")
    writeYaml file: ".${pathTo}", overwrite: true, data: data
}

def upgradeHelm(String releaseName, String chartPath, String namespace) {
    log.info("starting search in present cluster for releaseName: ${releaseName} in namespace ${namespace}")
    out = sh(returnStdout: true, script: "helm list --filter ${releaseName} -n ${namespace}").trim()
    if (!isContainRelease(out, releaseName)) {
        log.info("starting install helm for release name: ${releaseName} for chart path: ${chartPath} for namespace ${namespace}")
        sh "helm install --namespace ${namespace} -f .${chartPath}/values.yaml ${releaseName} .${chartPath}"
    } else {
        log.info("starting upgrade helm for release name: ${releaseName} for chart path: ${chartPath} for namespace ${namespace}")
        sh "helm upgrade --namespace ${namespace} -f .${chartPath}/values.yaml ${releaseName} .${chartPath}"
    }
}