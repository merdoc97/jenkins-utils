def call(Map pipelineParams) {
    pipeline {
        agent {
            label 'kubernetes-jenkins'
        }
        environment {
            JAVA_HOME = "/home/jenkins/jdk-13"
            M2_HOME = "${tool 'maven-3.6.3'}"
            PATH = "${env.JAVA_HOME}/bin:${env.PATH}"
            branch = "${pipelineParams.branch}"
            scmUrl = "${pipelineParams.scmUrl}"
            credentialsId = "jenkins"
            mavenModule = "${pipelineParams.mavenModule}"
            generateReport = "${pipelineParams.generateReport}"
            sonarPluginName = "sonar"
            useSonar = 'true'
        }

        stages {
            stage('Check out') {
                steps {
                    script {
                        git.gitCheckout("${env.branch}", "${env.credentialsId}", "${env.scmUrl}")
                    }
                }
            }
            stage('Maven build') {
                steps {
                    script {
                        maven.mavenPackage("${env.mavenModule}")
                    }
                }
            }
            stage('Maven verify') {
                steps {
                    script {
                        maven.mavenVerify("${env.mavenModule}")
                    }
                }
            }
            stage('Maven test') {
                steps {
                    script {
                        maven.mavenTest("${env.mavenModule}")
                    }
                }
            }
            stage('Generate test report') {
                steps {
                    script {
                        jacoco.generateReport("${env.mavenModule}", "${env.generateReport}")
                    }
                }
            }
            stage('Sonar quality gates') {
                steps {
                    script {
                        if ("${env.useSonar}" == 'true')
                            sonar.sonarAnalystAndQualityGates("${env.WORKSPACE}", "${env.mavenModule}", "${env.sonarPluginName}")
                    }
                }
            }
            stage('Deploy snapshot artifact') {
                steps {
                    script {
                        maven.deployToNexus("${env.mavenModule}")
                    }
                }
            }

        }
    }
}