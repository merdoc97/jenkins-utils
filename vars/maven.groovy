def mavenPackage(String module) {
    if (module != null && !module.isEmpty() && module != "null") {
        log.info("start package with module ${module}")
        sh "mvn clean package -DskipTests=true -pl ${module}"
    } else {
        log.info("start package without module")
        sh "mvn clean package -DskipTests=true"
    }
}

def mavenTest(String module) {
    if (module != null && !module.isEmpty() && module != "null") {
        log.info("start test with module ${module}")
        sh "mvn test -pl ${module}"
    } else {
        log.info("start test without module")
        sh "mvn test"
    }
}

def mavenVerify(String module) {
    if (module != null && !module.isEmpty() && module != "null") {
        log.info("start verify with module ${module}")
        sh "mvn verify -DskipTests=true -pl ${module}"
    } else {
        log.info("start verify without module ${module}")
        sh "mvn verify -DskipTests=true"
    }
}

def pushToRegistry(String module, String registryUrl, String tag) {
    log.info("start pushing docker image to registry for module: ${module} registry: ${registryUrl} with tag ${tag}")
    if (module != null && !module.isEmpty() && module != "null") {
        sh "mvn dockerfile:push -DregistryUrl=${registryUrl} -DdockerTag=${tag} -pl ${module}"
    } else {
        sh "mvn dockerfile:push -DregistryUrl=${registryUrl} -DdockerTag=${tag}"
    }
}

def buildDockerImage(String module, String registryUrl, String tag) {
    log.info("start building docker image for module: ${module} registry: ${registryUrl} with tag ${tag}")
    if (module != null && !module.isEmpty() && module != "null") {
        sh "mvn dockerfile:build -DregistryUrl=${registryUrl} -DdockerTag=${tag} -pl ${module}"
    } else {
        sh "mvn dockerfile:build -DregistryUrl=${registryUrl} -DdockerTag=${tag}"
    }
}

def deployToNexus(String module) {
    log.info("deploy artifact to nexus")
    if (module != null && !module.isEmpty() && module != "null") {
        sh "mvn deploy -DskipTests=true -pl ${module}"
    } else {
        sh "mvn deploy -DskipTests=true"
    }
}

def getCurrentSnapshotVersion(String module) {
    log.info("start getting version from module: ${module}")
    if (module != null && !module.isEmpty() && module != "null") {
        currentVersion = sh(returnStdout: true, script: "mvn help:evaluate -Dexpression=project.version -pl ${module} -q -DforceStdout").trim()
        log.info("current version is ${currentVersion}")
        if (!currentVersion.contains("-SNAPSHOT")) {
            error "Pipeline aborted due to version not contain -SNAPSHOT ending : ${currentVersion}"
        }
        return currentVersion
    } else {
        currentVersion = sh(returnStdout: true, script: "mvn help:evaluate -Dexpression=project.version -q -DforceStdout").trim()
        log.info("current version is ${currentVersion}")
        if (!currentVersion.contains("-SNAPSHOT")) {
            error "Pipeline aborted due to version not contain -SNAPSHOT ending : ${currentVersion}"

        }
        return currentVersion
    }
}

def splitVersionSnapshot(String currentVersion) {
    log.info("split snapshot version ${currentVersion}")
    if (!currentVersion.contains("-SNAPSHOT")) {
        error "Split snapshot version not posiblle current version not contain -SNAPSHOT ending : ${currentVersion}"
    }
    return currentVersion.split("-SNAPSHOT")[0].trim()
}

def incrementVersion(String versionWithoutSnapshot) {
    String[] versions = versionWithoutSnapshot.split("\\.")
    lastNumber = Integer.parseInt(versions[versions.length - 1])
    log.info("last number from version is ${lastNumber}")
    newVersion = null
    for (int i = 0; i < versions.length - 1; i++) {
        newVersion = versions[i] + "."
    }
    return newVersion + (++lastNumber)
}

def changeSnapshotVersion(String newVersion, String module) {
    snapShotVersion = newVersion + "-SNAPSHOT"
    if (module != null && !module.isEmpty() && module != "null") {
        log.info("updating new shanpshot version in module ${module}, new snapshot version is ${snapShotVersion}")
        sh(returnStdout: true, script: "mvn versions:set -DnewVersion=${snapShotVersion} -pl ${module}")
    } else {
        log.info("updating new shanpshot version in project, new snapshot version is ${snapShotVersion}")
        sh(returnStdout: true, script: "mvn versions:set -DnewVersion=${snapShotVersion}")
    }
}

def changeVersion(String newVersion, String module) {
    if (module != null && !module.isEmpty() && module != "null") {
        log.info("updating new shanpshot version in module ${module}, new snapshot version is ${newVersion}")
        sh(returnStdout: true, script: "mvn versions:set -DnewVersion=${newVersion} -pl ${module}")
    } else {
        log.info("updating new shanpshot version in project, new snapshot version is ${newVersion}")
        sh(returnStdout: true, script: "mvn versions:set -DnewVersion=${newVersion}")
    }
}

def getCurrentNotSnapshot(String module) {
    log.info("start getting version from module: ${module}")
    if (module != null && !module.isEmpty() && module != "null") {
        currentVersion = sh(returnStdout: true, script: "mvn help:evaluate -Dexpression=project.version -pl ${module} -q -DforceStdout").trim()
        log.info("current version is ${currentVersion}")
        if (currentVersion.contains("-SNAPSHOT")) {
            error "Pipeline aborted due to version not contain -SNAPSHOT ending : ${currentVersion}"
        }
        return currentVersion
    } else {
        currentVersion = sh(returnStdout: true, script: "mvn help:evaluate -Dexpression=project.version -q -DforceStdout").trim()
        log.info("current version is ${currentVersion}")
        if (currentVersion.contains("-SNAPSHOT")) {
            error "Pipeline aborted due to version not contain -SNAPSHOT ending : ${currentVersion}"

        }
        return currentVersion
    }
}