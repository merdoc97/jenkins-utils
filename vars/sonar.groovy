def sonarAnalystAndQualityGates(String workSpaceDir, String module, String sonarPluginName) {
    if (mavenModule != null && !module.isEmpty() && module != "null") {
        withSonarQubeEnv(sonarPluginName) {
            log.info("start sonar analyst for workdir ${workSpaceDir} and module ${mavenModule}")
            sh "mvn -f ${workSpaceDir}/${mavenModule}pom.xml sonar:sonar"
        }
    } else {
        withSonarQubeEnv(sonarPluginName) {
            log.info("start sonar analyst for workdir ${workSpaceDir}")
            sh "mvn sonar:sonar"
        }
    }
    sleep(10)
    def gateResponse = waitForQualityGate()
    log.info("get response from sonar server status: ${gateResponse.status}")
    if (gateResponse.status != 'OK') {
        error "Pipeline aborted due to quality gate failure: ${gateResponse.status}"
    }
}