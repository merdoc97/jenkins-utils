
def buildWithArgs(String moduleName, String dockerFileName, String imageName, String argName) {
    log.info("Starting build docker image for module ${moduleName}, from ${dockerFileName} with ${imageName}")
    sh "docker build --build-arg ${argName}=${moduleName} -t ${imageName} -f ${dockerFileName} ."
}

def addTagToLatestImage(String imageName, String registryUrl, String tagName) {
    log.info("Start add tag=${tagName} to latest image=${imageName} and for registry=${registryUrl}")
    sh "docker tag ${imageName}:latest ${registryUrl}/${imageName}:${tagName}"
}

def pushImageToRegistry(String registryUrl, String imageName, String tagName) {
    log.info("Start push image to registry ${registryUrl}/${imageName}:${tagName}")
    sh "docker push ${registryUrl}/${imageName}:${tagName}"
}