/**
 *
 * checkout via ssh
 *
 **/

def gitCheckout(String branch, String sshCredentialsId, String scmUrl) {
    log.info("start check out for branch: ${branch} with credentials: ${sshCredentialsId} from scmUrl: ${scmUrl}")
    checkout([
            $class                           : 'GitSCM',
            branches                         : [[name: '*/' + branch]],
            doGenerateSubmoduleConfigurations: false,
            extensions                       : [[$class: 'CleanCheckout']],
            submoduleCfg                     : [],
            userRemoteConfigs                : [[credentialsId: sshCredentialsId, url: scmUrl]]
    ])
}

def getLastCommit() {
    commit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
    log.info("latest git commit: ${commit}")
    return commit
}

def checkout(String credentialId, String branchName) {
    log.info("checkout to branch: ${branchName}")
    sshagent(credentials: [credentialId]) {
        sh("git checkout ${branchName}")
    }
}

def createReleaseBranchAndPush(String credentialId, String version, String currentBranchName) {
    releaseVersion = version + "-RELEASE"
    log.info("create new local branch: ${releaseVersion}")
    sshagent(credentials: [credentialId]) {
        sh("git checkout -b ${releaseVersion}")
    }
    push(credentialId, releaseVersion)
    checkout(credentialId, currentBranchName)
    pull(credentialId)
}

def push(String credentialId, String branchName) {
    log.info("pushing new changes to remote branch: ${branchName}")
    sshagent(credentials: [credentialId]) {
        sh("git push origin HEAD:${branchName}")
    }
}

def pull(String credentialId) {
    log.info("oull changes to remote branch: ${branchName}")
    sshagent(credentials: [credentialId]) {
        sh("git pull origin")
    }
}

def commit(String message) {
    log.info("create commit with message ${message}")
    sh(returnStdout: false, script: "git config user.email 'jenkins@gmail.com'")
    sh(returnStdout: false, script: "git config user.name 'jenkins'")
    sh("git commit -a -m '${message}'")
}

def addTag(String tag) {
    log.info("create tag ${tag}")
    commit = getLastCommit()
    sh(returnStdout: false, script: "git config user.email 'jenkins@gmail.com'")
    sh(returnStdout: false, script: "git config user.name 'jenkins'")
    sh("git tag '${tag}' " + commit)
}

def pushTag(String credentialId) {
    sshagent(credentials: [credentialId]) {
        sh("git push --tags")
    }
}