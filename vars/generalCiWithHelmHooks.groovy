def call(Map pipelineParams) {
    pipeline {
        agent {
            label 'kubernetes-jenkins'
        }
        environment {
            JAVA_HOME = "/home/jenkins/jdk-13"
            M2_HOME = "${tool 'maven-3.6.3'}"
            DOCKER_HOME="/home/jenkins/docker"
            PATH = "${env.JAVA_HOME}/bin:${env.DOCKER_HOME}:${env.PATH}"
            DOCKER_OPTS="-H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock"
            branch = "${pipelineParams.branch}"
            scmUrl = "${pipelineParams.scmUrl}"
            credentialsId = "jenkins"
            mavenModule = "${pipelineParams.mavenModule}"
            dockerRegistry = "localhost:5000"
            pathToAppChart = "${pipelineParams.pathToAppChart}"
            helmReleaseName = "${pipelineParams.helmReleaseName}"
            kubernetesNameSpace = "${pipelineParams.kubernetesNameSpace}"
            generateReport = "${pipelineParams.generateReport}"
            sonarPluginName = "sonar"
            useSonar = 'false'
            dockerHookFile = "${pipelineParams.dockerHookFile}"
            hookArgName = "${pipelineParams.hookArgName}"
            hookSuffix = "-db-hook"
        }

        stages {
            stage('Check out') {
                steps {
                    script {
                        git.gitCheckout("${env.branch}", "${env.credentialsId}", "${env.scmUrl}")
                    }
                }
            }
            stage('Maven build') {
                steps {
                    script {
                        maven.mavenPackage("${env.mavenModule}")
                    }
                }
            }
            stage('Maven test') {
                steps {
                    script {
                        maven.mavenTest("${env.mavenModule}")
                    }
                }
            }
            stage('Generate test report') {
                steps {
                    script {
                        jacoco.generateReport("${env.mavenModule}", "${env.generateReport}")
                    }
                }
            }
            stage('Sonar analyst and quality gate') {
                steps {
                    script {
                        if ("${env.useSonar}" == 'true')
                            sonar.sonarAnalystAndQualityGates("${env.WORKSPACE}", "${env.mavenModule}", "${env.sonarPluginName}")
                    }
                }
            }
            stage('Maven docker build') {
                steps {
                    script {
                        maven.buildDockerImage("${env.mavenModule}", "${env.dockerRegistry}", git.getLastCommit())
                    }
                }
            }
            stage('Maven push to registry') {
                steps {
                    script {
                        maven.pushToRegistry("${env.mavenModule}", "${env.dockerRegistry}", git.getLastCommit())
                    }
                }
            }
            stage('Upgrade version in chart') {
                steps {
                    script {
                        helm.upgradeChartVersion("${env.pathToAppChart}/Chart.yaml", git.getLastCommit())
                    }
                }
            }
            stage('Prepare image for db helm hook') {
                steps {
                        script {
                            log.info("start preparing image name for hook")
                            def moduleName = "${env.mavenModule}".tokenize("/")[0]
                            def imageName = moduleName + "${env.hookSuffix}"
                            log.info("finish preparing image name for hook with name $imageName")
                            log.info("module name is $moduleName")
                            def imageTag = git.getLastCommit()
                            log.info("image tag is $imageTag")
                            def hookFile = "${env.dockerHookFile}".toString()
                            def hookArgName = "${env.hookArgName}".toString()
                            dockerUtil.buildWithArgs(moduleName, hookFile, imageName, hookArgName)
                            dockerUtil.addTagToLatestImage("${imageName}", "${env.dockerRegistry}", "${imageTag}")
                            dockerUtil.pushImageToRegistry("${env.dockerRegistry}", "${imageName}", "${imageTag}")
                        }
                }
            }
            stage('Helm upgrade') {
                steps {
                    script {
                        helm.upgradeHelm("${env.helmReleaseName}", "${env.pathToAppChart}", "${env.kubernetesNameSpace}")
                    }
                }
            }

        }
    }

}