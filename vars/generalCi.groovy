def call(Map pipelineParams) {
    pipeline {
        agent {
            label 'kubernetes-jenkins'
        }
        environment {
            JAVA_HOME = "/home/jenkins/jdk-13"
            M2_HOME = "${tool 'maven-3.6.3'}"
            PATH = "${env.JAVA_HOME}/bin:${env.PATH}"
            branch = "${pipelineParams.branch}"
            scmUrl = "${pipelineParams.scmUrl}"
            credentialsId = "jenkins"
            mavenModule = "${pipelineParams.mavenModule}"
            dockerRegistry = "localhost:5000"
            pathToAppChart = "${pipelineParams.pathToAppChart}"
            helmReleaseName = "${pipelineParams.helmReleaseName}"
            kubernetesNameSpace = "${pipelineParams.kubernetesNameSpace}"
            generateReport = "${pipelineParams.generateReport}"
            sonarPluginName = "sonar"
            useSonar = 'false'
        }

        stages {
            stage('Check out') {
                steps {
                    script {
                        git.gitCheckout("${env.branch}", "${env.credentialsId}", "${env.scmUrl}")
                    }
                }
            }
            stage('Maven build') {
                steps {
                    script {
                        maven.mavenPackage("${env.mavenModule}")
                    }
                }
            }
            stage('Maven test') {
                steps {
                    script {
                        maven.mavenTest("${env.mavenModule}")
                    }
                }
            }
            stage('Generate test report') {
                steps {
                    script {
                        jacoco.generateReport("${env.mavenModule}", "${env.generateReport}")
                    }
                }
            }
            stage('Sonar analyst and quality gate') {
                steps {
                    script {
                        if ("${env.useSonar}" == 'true')
                            sonar.sonarAnalystAndQualityGates("${env.WORKSPACE}", "${env.mavenModule}", "${env.sonarPluginName}")
                    }
                }
            }
            stage('Maven docker build') {
                steps {
                    script {
                        maven.buildDockerImage("${env.mavenModule}", "${env.dockerRegistry}", git.getLastCommit())
                    }
                }
            }
            stage('Maven push to registry') {
                steps {
                    script {
                        maven.pushToRegistry("${env.mavenModule}", "${env.dockerRegistry}", git.getLastCommit())
                    }
                }
            }
            stage('Upgrade version in chart') {
                steps {
                    script {
                        helm.upgradeChartVersion("${env.pathToAppChart}/Chart.yaml", git.getLastCommit())
                    }
                }
            }
            stage('Helm upgrade') {
                steps {
                    script {
                        helm.upgradeHelm("${env.helmReleaseName}", "${env.pathToAppChart}", "${env.kubernetesNameSpace}")
                    }
                }
            }

        }
    }

}